/* eslint-disable no-undef */
import { expect } from 'chai';

import {
  buildMap,
  buildObjectArray,
  findMines,
  findSafeBlocks
} from '../src/utils';

const testCase1 = `map:
□□□□□□□□□□
□□□□□□□□□□
□□□□□□□□□□
□□□□□□□□□□
□□□□□□□□□□
□□□□□1□□□□
□□□□□□□□□□
□□□□□□□□□□
□□□□□□□□□□
□□□□□□□□□□
`;

const testCase2 = `map:
□□□□□10000
□□□□□10000
□□11110122
□□100001□□
□□110112□□
□□□101□□□□
□□□211□□□□
□□□□□□□□□□
□□□□□□□□□□
□□□□□□□□□□
`;

describe('buildMap', () => {
  it('should build map from send data', () => {
    const result = buildMap(testCase1);
    expect(result.length).to.be.equal(10);
    expect(result[0].length).to.be.equal(10);
  });
});

describe('buildObjectArray', () => {
  it('should convert map to a map with objects', () => {
    const X = 5;
    const Y = 5;
    const numberOfMines = 1;

    const map = buildMap(testCase1);
    const objectArray = buildObjectArray(map);

    expect(objectArray.length).to.be.equal(10);
    expect(objectArray[0].length).to.be.equal(10);
    expect(objectArray[Y][X]).to.deep.equal({
      numberOfMines,
      isMine: false,
      isSafe: false,
      safety: 0,
      y: Y,
      x: X
    });
  });
});

describe('findMines', () => {
  it('should find all the blocks that are 100% mines', () => {
    const X = 4;
    const Y = 1;
    const map = buildMap(testCase2);
    const objectArray = buildObjectArray(map);
    const mineMap = findMines(objectArray);

    expect(mineMap.length).to.be.equal(10);
    expect(mineMap[0].length).to.be.equal(10);
    expect(mineMap[Y][X].isMine).to.equal(true);
  });
});

describe('findSafeBlocks', () => {
  it('should return array with all the blocks that are 100% safe to open', () => {
    const map = buildMap(testCase2);
    const objectArray = buildObjectArray(map);
    const mineMap = findMines(objectArray);
    const { safeBlocks } = findSafeBlocks(mineMap);
    expect(safeBlocks.length).to.be.equal(14);
    expect(safeBlocks[0].isSafe).to.equal(true);
  });

  it('should return array lowRiskBlocks and empty safeBlocks', () => {
    const map = buildMap(testCase1);
    const objectArray = buildObjectArray(map);
    const mineMap = findMines(objectArray);
    const { safeBlocks, lowRiskBlocks } = findSafeBlocks(mineMap);
    expect(safeBlocks.length).to.be.equal(0);
    expect(lowRiskBlocks.length).to.be.equal(8);
    expect(lowRiskBlocks[0].safety).to.equal(0.125);
  });
});
