import 'regenerator-runtime/runtime';

import React from 'react';
import ReactDOM from 'react-dom';
import { createGlobalStyle } from 'styled-components';

import App from './components/App';

const GlobalStyle = createGlobalStyle`
	body, html, #container {
		padding: 0;
		height: 100%;
		width: 100%;
		margin: 0;
	}
`;

const Container = () => (
  <>
    <GlobalStyle />
    <App />
  </>
);

export default Container;

const wrapper = document.getElementById('container');
wrapper ? ReactDOM.render(<Container />, wrapper) : false;
