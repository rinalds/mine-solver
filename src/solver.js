import { buildMap, buildObjectArray, findMines, findSafeBlocks } from './utils';

export const calculateNextMoves = data => {
  const rawMap = buildMap(data);
  const map = buildObjectArray(rawMap);

  const minedObjectMap = findMines(map);
  const { safeBlocks, lowRiskBlocks } = findSafeBlocks(minedObjectMap);
  //   this.objectMap = objectMap;
  //   this.lowRiskBlocks = lowRiskBlocks;
  //   this.safeBlocks = safeBlocks;

  if (safeBlocks.length > 0) {
    return safeBlocks;
  }
  return [lowRiskBlocks[0]];
};

export const buildFullMap = data => {
  const rawMap = buildMap(data);
  const map = buildObjectArray(rawMap);

  const minedObjectMap = findMines(map);
  const { objectMap } = findSafeBlocks(minedObjectMap);

  return objectMap;
};

export default {};
