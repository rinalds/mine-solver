import styled from 'styled-components';

const Message = styled.h2`
  font-family: sans-serif;
  font-weight: normal;
  margin: 30px 0;
  text-align: center;
`;

export default Message;
