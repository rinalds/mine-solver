import React from 'react';
import styled from 'styled-components';

const LEVELS = 4;

const LevelSelector = ({ className, selectedLevel, onLevelSelect }) => {
  const renderLevels = levels => {
    const levelButtons = [];
    for (let i = 1; i <= levels; i += 1) {
      levelButtons.push(
        <button
          key={i}
          type="button"
          onClick={() => onLevelSelect(i)}
        >{`LEVEL ${i}`}</button>
      );
    }
    return levelButtons;
  };

  return <div className={className}>{renderLevels(LEVELS)}</div>;
};

const StyledLevelSelector = styled(LevelSelector)`
  button {
    border: 1px solid #00000038;
    font-size: 18px;
    font-weight: bold;
    padding: 10px;
    border-right-width: 0;
    cursor: pointer;
    outline: none;
    transition: 0.3s all;

    &:hover {
      background: #f7f7f7;
    }

    &:last-child {
      border-right-width: 1px;
      border-radius: 0 4px 4px 0;
    }
    &:first-child {
      border-radius: 4px 0 0 4px;
    }

    &:nth-child(${props => props.selectedLevel}) {
      background: #e4e4f7;
    }
  }
`;

export default StyledLevelSelector;
