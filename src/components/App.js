import React, { Component } from 'react';
import styled from 'styled-components';

import Row from './Row';
import Block from './Block';
import LevelSelector from './LevelSelector';
import StartButton from './StartButton';
import Message from './Message';
import MapContainer from './MapContainer';

import Api from '../api';
import { calculateNextMoves, buildFullMap } from '../solver';

const GameStates = {
  SELECT_LEVEL: 'SELECT_LEVEL',
  PLAYING: 'PLAYING',
  LOST: 'LOST',
  WON: 'WON'
};

class App extends Component {
  constructor() {
    super();

    this.lowRiskBlocks = [];
    this.safeBlocks = [];

    this.state = {
      level: 1,
      gameState: GameStates.SELECT_LEVEL,
      objectMap: null,
      message: null,
      numberOfAttempts: 0,
      movesMade: 0
    };

    this.api = new Api();
  }

  setGameLost = () => {
    const { numberOfAttempts } = this.state;
    this.setState({
      movesMade: 0,
      numberOfAttempts: numberOfAttempts + 1
    });
  };

  setGameWon = async data => {
    const map = await this.api.fetchMap();
    const objectMap = buildFullMap(map);
    this.setState({
      message: data,
      gameState: GameStates.SELECT_LEVEL,
      objectMap
    });
  };

  startGameLoop = async () => {
    // Make Initial move
    let moveState = await this.api.makeMove({ x: 5, y: 5 });
    let mapData = null;

    while (
      this.state.gameState === GameStates.PLAYING &&
      moveState === 'open: OK'
    ) {
      mapData = await this.api.fetchMap();
      let nextMoves = calculateNextMoves(mapData);
      const movesMade = nextMoves.length;

      if (nextMoves.length > 1) {
        while (nextMoves.length > 0) {
          moveState = await this.api.makeMove(nextMoves[0]);
          nextMoves = nextMoves.slice(1);
        }
      } else {
        moveState = await this.api.makeMove(nextMoves[0]);
      }

      this.setState({ movesMade: this.state.movesMade + movesMade });
    }

    return moveState;
  };

  playGame = async () => {
    const { level } = this.state;

    await this.setState({
      gameState: GameStates.PLAYING,
      message: null,
      movesMade: 0
    });

    const newGameStatus = await this.api.newGame(level);

    if (newGameStatus !== 'new: OK') {
      return this.setState({
        gameState: GameStates.SELECT_LEVEL,
        message: 'PROBLEM WITH STARTING NEW GAME'
      });
    }

    const moveState = await this.startGameLoop();

    if (moveState === 'open: You lose') {
      this.setGameLost();
      this.playGame();
    } else if (moveState.startsWith('open: You win')) {
      this.setGameWon(moveState);
    }

    return true;
  };

  start = async () => {
    const { gameState } = this.state;

    if (gameState === GameStates.PLAYING) {
      this.setState({ gameState: GameStates.SELECT_LEVEL });
    } else {
      this.playGame();
    }
  };

  render() {
    const {
      objectMap,
      message,
      level,
      gameState,
      numberOfAttempts,
      movesMade
    } = this.state;
    const { className } = this.props;

    return (
      <div className={className}>
        <LevelSelector
          selectedLevel={level}
          onLevelSelect={newLevel => this.setState({ level: newLevel })}
        />
        <StartButton type="button" onClick={this.start}>
          {gameState === GameStates.SELECT_LEVEL ? 'START' : 'STOP'}
        </StartButton>

        {message && <Message>{message}</Message>}
        {gameState === GameStates.PLAYING && (
          <Message>{`Moves made in game: ${movesMade}, Number of attempts: ${numberOfAttempts}`}</Message>
        )}

        {objectMap && <Message>Map from last game:</Message>}
        {objectMap && (
          <MapContainer>
            {objectMap.map((row, y) => {
              return (
                <Row key={y}>
                  {row.map((block, x) => {
                    return (
                      <Block block={block} key={x}>
                        {block.isMine ? 'X' : block.numberOfMines}
                      </Block>
                    );
                  })}
                </Row>
              );
            })}
          </MapContainer>
        )}
      </div>
    );
  }
}

const StyledApp = styled(App)`
  box-sizing: border-box;
  display: flex;
  flex-direction: column;
  width: 100%;
  height: 100%;
  background: #f5f5ff;
  align-items: center;
  padding: 40px 20px;
`;

export default StyledApp;
