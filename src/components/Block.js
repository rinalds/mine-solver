import styled from 'styled-components';

const Block = styled.div`
  border: 1px solid #8e8e8e;
  padding: 0px;
  width: 20px;
  height: 20px;
  outline: none;
  box-shadow: inset 0px 0px 0px 3px #00000021;
  font-family: sans-serif;
  text-align: center;
  flex: 1 0 20px;
  background: ${props => {
    if (props.block.isMine) {
      return '#e86060';
    }
    if (props.block.safety > 0) {
      return `rgba(0,128,0, ${props.chance > 1 ? 1 : props.chance})`;
    }
    if (props.clear) {
      return '#9e9e9e';
    }
    if (props.block.isSafe) {
      return 'blue';
    }
    return '#f7f7f7';
  }};
`;

export default Block;
