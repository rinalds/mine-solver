import styled from 'styled-components';

const StyledStartButton = styled.button`
  border: 1px solid #00000038;
  font-size: 18px;
  font-weight: bold;
  padding: 10px;
  cursor: pointer;
  outline: none;
  transition: 0.3s all;
  border-radius: 4px;
  margin-top: 15px;
  max-width: 250px;
  width: 100%;

  &:hover {
    background: #f7f7f7;
  }
`;

export default StyledStartButton;
