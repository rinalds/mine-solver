import styled from 'styled-components';

const MapContainer = styled.div`
  overflow: auto;
  width: 100%;
`;

export default MapContainer;
