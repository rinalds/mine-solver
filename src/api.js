export default class Api {
  constructor() {
    this.eventListeners = {};
    this.queue = null;

    this.socket = new WebSocket('ws://hometask.eg1236.com/game1/');

    this.socket.onopen = () => {
      console.log('Connection Open');
    };

    this.socket.onerror = error => {
      console.log('Error', error);
    };

    // this.socket.onmessage = ({ data }) => {
    //   console.log('Message from server ', data);
    // };
  }

  fetchMap = () => {
    const promise = new Promise((resolve, reject) => {
      this.socket.send('map');

      const callback = ({ data }) => {
        if (data.startsWith('map')) {
          resolve(data);
        } else {
          reject(new Error('Received wrong response from server'));
        }
        this.socket.removeEventListener('message', callback);
      };

      this.socket.addEventListener('message', callback);
    });
    return promise;
  };

  makeMove = block => {
    const promise = new Promise((resolve, reject) => {
      this.socket.send(`open ${block.x} ${block.y}`);

      const callback = ({ data }) => {
        if (data.startsWith('open:')) {
          resolve(data);
        } else {
          reject(new Error('Received wrong response from server'));
        }
        this.socket.removeEventListener('message', callback);
      };

      this.socket.addEventListener('message', callback);
    });
    return promise;
  };

  newGame = level => {
    const promise = new Promise((resolve, reject) => {
      this.socket.send(`new ${level}`);

      const callback = ({ data }) => {
        if (data.startsWith('new: OK')) {
          resolve(data);
        } else {
          reject(new Error('Received wrong response from server'));
        }
        this.socket.removeEventListener('message', callback);
      };

      this.socket.addEventListener('message', callback);
    });
    return promise;
  };
}
