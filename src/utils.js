/* eslint-disable import/prefer-default-export */

export const buildMap = data => {
  return data
    .split(/\n/)
    .slice(1, -1)
    .map(row => row.split(''));
};

export const buildObjectArray = rawMap => {
  return rawMap.map((row, y) =>
    row.map((block, x) => {
      return {
        numberOfMines: /^[0-9]$/.test(block)
          ? Number.parseInt(block, 10)
          : null,
        isMine: false,
        isSafe: false,
        safety: 0,
        y,
        x
      };
    })
  );
};

export const findMines = objectMap => {
  const map = JSON.parse(JSON.stringify(objectMap));

  map.forEach((row, y) => {
    row.forEach((block, x) => {
      if (!block.numberOfMines) {
        return;
      }

      let radiusSize = 9;
      const hiddenBlocks = [];

      for (let y2 = y - 1; y2 < y + 2; y2 += 1) {
        for (let x2 = x - 1; x2 < x + 2; x2 += 1) {
          if (map[y2] && map[y2][x2]) {
            const rBlock = map[y2][x2];

            if (rBlock.numberOfMines === null) {
              hiddenBlocks.push(rBlock);
            } else {
              radiusSize -= 1;
            }
          } else {
            radiusSize -= 1;
          }
        }
      }
      if (radiusSize === block.numberOfMines) {
        hiddenBlocks.forEach(b => {
          b.isMine = true;
        });
      }
    });
  });
  return map;
};

export const findSafeBlocks = objectMap => {
  const lowRiskBlocks = [];
  const safeBlocks = [];
  const map = JSON.parse(JSON.stringify(objectMap));

  map.forEach((row, y) => {
    row.forEach((block, x) => {
      if (block.numberOfMines > 0) {
        let minesInRadius = 0;
        const tempSafeBlocks = [];

        for (let i = y - 1; i < y + 2; i += 1) {
          for (let j = x - 1; j < x + 2; j += 1) {
            if (map[i] && map[i][j]) {
              const rBlock = map[i][j];
              if (rBlock.isMine) {
                minesInRadius += 1;
              } else {
                tempSafeBlocks.push(rBlock);
              }
            }
          }
        }
        if (minesInRadius === block.numberOfMines) {
          tempSafeBlocks.forEach(b => {
            if (b.numberOfMines === null) {
              b.isSafe = true;
              if (safeBlocks.indexOf(b) === -1) {
                safeBlocks.push(b);
              }
            }
          });
        } else {
          tempSafeBlocks.forEach(b => {
            if (b.numberOfMines === null) {
              b.safety = block.numberOfMines / (tempSafeBlocks.length - 1);
              if (lowRiskBlocks.indexOf(b) === -1) {
                lowRiskBlocks.push(b);
              }
            }
          });
        }
      }
    });
  });

  return {
    objectMap: map,
    safeBlocks,
    lowRiskBlocks: lowRiskBlocks.sort((a, b) => a.safety - b.safety)
  };
};
